#!/bin/bash
GREEN="\033[32m"
NORMAL="\033[0;39m"
FOLDER=$(date +"%Y-%m-%d")
mkdir $FOLDER
echo -e "$GREEN"
echo "Exporting clients"
node ./clientlist/userlist.js > ${FOLDER}/clients.csv
echo "Exporting freelancers"
node ./freelancerlist/userlist.js > ${FOLDER}/freelancer.csv
echo "Exporting payments"
node ./payments/payments.js > ${FOLDER}/payments.csv
echo "Exporting Timesheets"
node ./timesheets/timesheets.js > ${FOLDER}/tasks.csv
echo -e "Done! $NORMAL"
