var MongoClient = require('mongodb').MongoClient; // Driver for connecting to MongoDB
var MONGO_URL = 'mongodb://webapp:sgdfGFAasheq245ssd32@c1.cockney.2.mongolayer.com:10001,c1.cockney.3.mongolayer.com:10001/taskwunder-webapp?replicaSet=set-55131972a7878c7b3e000048';


MongoClient.connect(MONGO_URL, function(err, db) {

    if(err) throw err;
    var users = db.collection("users");
    var tasks = db.collection("tasks");

    var user = users.find({"roles":"client"}).sort({_id:1}).toArray(function(err,users){
        if(err) throw err;
        console.log("ID;Email;First name;Last name;Language;Signup Date;Last login;Gets email;Balance;Has PA;Has PM;VIP;Lifetime spend;Last Task Created;Account Type;Prosaldo;Company Name;Country; City; Zipcode; Street; Tax ID");

        var findLastTask = function(user){
          tasks.find({creator:user._id},{createdAt:1,publicPrice:1}).sort({createdAt:-1}).toArray(function(err,tasks){
            if(err) throw err;

              var result = "";
              result += user._id + ";";
              result += user.emails[0].address + ";";

              // Name
              result += user.profile.name + ";";
              result += user.profile.lastName + ";";

              //Language
              result += user.profile.language + ";";

              // Signup date
              result += user.createdAt + ";";

              // Last Login date
              result += user.status.lastLogin ? user.status.lastLogin.date + ";" : ";";

              // Gets email
              result += user.data.noEmail ? "No" : "Yes";
              result += ";";

              // Balance
              result += user.profile.balance + ";";

              // check if user has PA
              result += user.profile.worksWith[0] ? "Yes" : "No";
              result += ";";

              // check if user has PM
              if (user.data.team) {
              result += user.data.team[0] ? "Yes" : "No";
              }
              result += ";";

              // check if user is vip
              var vip;
              user.roles.forEach(function(role){
                if (role === "vip") {
                  vip = true;
                }
              });
              result += vip ? "vip" : " ";
              result += ";";

              // Calculate Lifetime hours spent
              var sum = 0;
              tasks.forEach(function(task){
                var earn;
                earn =  parseFloat(task.publicPrice);
                sum = sum + (earn * 1 );
              });

              result += sum + ";";

              // Date of last task
               if (tasks.length > 0) {
                 result += tasks[0].createdAt;
               }
              result +=";";

              // Account type
              result += user.data.personType + ";";

          		// Prosaldo
              if (user.additionalInfo && user.additionalInfo[0]) {
                  result += user.additionalInfo[0].value + ";";
              }

              //if there is a company append it
              if (user.data.companyInfo.companyName) {
                result += user.data.companyInfo.companyName ? user.data.companyInfo.companyName.replace(/,/g, " ")+ ";" : ";";
                result += user.data.companyInfo.country ? user.data.companyInfo.country.replace(/,/g, " ") + ";" : ";";
                result += user.data.companyInfo.city ? user.data.companyInfo.city.replace(/,/g, " ") + ";" : ";";
                result += user.data.companyInfo.zipcode + ";";
                result += user.data.companyInfo.street ? user.data.companyInfo.street.replace(/,/g, " ") + ";" : ";";
                result += user.data.companyInfo.taxId + ";";
              }



             console.log(result.toString("utf8"));

          });
        };


        users.forEach(function(user){

          // Get the time of the last created task
          findLastTask(user);

        });

        setTimeout(function(){
            db.close();
        }, 3600*1);

    });

});
