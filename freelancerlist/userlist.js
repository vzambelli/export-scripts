var MongoClient = require('mongodb').MongoClient; // Driver for connecting to MongoDB
var MONGO_URL = 'mongodb://webapp:sgdfGFAasheq245ssd32@c1.cockney.2.mongolayer.com:10001,c1.cockney.3.mongolayer.com:10001/taskwunder-webapp?replicaSet=set-55131972a7878c7b3e000048';


MongoClient.connect(MONGO_URL, function(err, db) {

    if(err) throw err;
    var users = db.collection("users");
    var tasks = db.collection("tasks");

    /*
    2:54
- last logged in date
- signed up date
- lifetime hours earned
- unpaid hours
- PA
- PM
- moderator
- language
- last task accepted
- last task completed
*/

    var user = users.find({"roles":"assistant"},{"data":1,"profile":1,"emails":1,"roles":1,"status.lastLogin":1,"createdAt":1}).sort({_id:1}).toArray(function(err,users){
        if(err) throw err;
        console.log("ID;Email;First Name;Last Name;Language;Skills;Standard Rate; Premium Rate;Signup date;Last Login date;PA;PM;Moderator;Last Task completed;Account Type;Company Name;Country; City; Zipcode; Street; Tax ID");

        var findLastTask = function(user){
          tasks.find({owner:user._id},{doneAt:1}).sort({doneAt:-1}).toArray(function(err,tasks){
            if(err) throw err;

              var result = "";
              result += user._id + ";";
              result += user.emails[0].address + ";";
              result += user.profile.name + ";";
              result += user.profile.lastName + ";";

              // Language
              result += user.profile.language + ";";

              // Skills

              var skills = "";
              user.profile.skills.forEach(function(skill){
                skills += skill + ";";
              });

              result += skills + ";";

              // Rates
              result += user.profile.hourlyRate + ";";
              result += user.data.premiumHourlyRate + ";";

              result += user.createdAt + ";";

              // Last Login date
              result += user.status.lastLogin ? user.status.lastLogin.date + ";" : ";";

              // check if user is vip
              var pa;
              var pm;
              var mod;
              user.roles.forEach(function(role){
                if (role === "pa") {
                  pa = true;
                }
                if (role === "pm") {
                  pm = true;
                }
                if (role === "moderator") {
                  mod = true;
                }
              });

              result += pa ? "PA" : " ";
              result += ";";
              result += pm ? "PM" : " ";
              result += ";";
              result += mod ? "Moderator" : "";
              result += ";";

               if (tasks.length > 0) {
                 result += tasks[0].doneAt;
               }
              result +=";";

              // Account type
              result += user.data.personType + ";";

              //if there is a company append it
              if (user.data.companyInfo.companyName) {
                result += user.data.companyInfo.companyName ? user.data.companyInfo.companyName.replace(/,/g, " ")+ ";" : ";";
                result += user.data.companyInfo.country ? user.data.companyInfo.country.replace(/,/g, " ") + ";" : ";";
                result += user.data.companyInfo.city ? user.data.companyInfo.city.replace(/,/g, " ") + ";" : ";";
                result += user.data.companyInfo.zipcode + ";";
                result += user.data.companyInfo.street ? user.data.companyInfo.street.replace(/,/g, " ") + ";" : ";";
                result += user.data.companyInfo.taxId + ";";
              }

             console.log(result.toString("utf8"));

          });
        };


        users.forEach(function(user){

          // Get the time of the last created task
          findLastTask(user);

        });

        setTimeout(function(){
            db.close();
        }, 3600*1);

    });

});
