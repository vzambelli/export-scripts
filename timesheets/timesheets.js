var MongoClient = require('mongodb').MongoClient; // Driver for connecting to MongoDB
var MONGO_URL = 'mongodb://webapp:sgdfGFAasheq245ssd32@c1.cockney.2.mongolayer.com:10001,c1.cockney.3.mongolayer.com:10001/taskwunder-webapp?replicaSet=set-55131972a7878c7b3e000048';


MongoClient.connect(MONGO_URL, function(err, db) {

    if(err) throw err;
    var tasksCollection = db.collection("tasks");
    var timesheets = db.collection("timesheets");

    /*
    2:54

*/

    var c = ";";

    var tasks = tasksCollection.find({}).sort({createdAt:-1}).toArray(function(err,tasks){
        if(err) throw err;
        console.log("ID;ClientID;Client;Category;createdAt;Pending;Done;Archived;Subject;Price Client Paid in Hours;Hours received by Freelancer;Payment in €;FreelancerID;Freelancer;Paid;On Hold;Needs Quote");

        var findTimesheets = function(task){
          timesheets.findOne({taskId:task._id},function(err,timesheet){
            if(err) throw err;

            var result = "";


            //ID
            result += task._id + c;

            //Client
            result += task.creator + c;
            result += task.clientName + c;

            //task
            result += task.category[0] + c;
            result += task.createdAt + c;
            result += task.pending + c;
            result += task.done + c;
            result += task.archived + c;
            result += task.subject + c;
            result += task.publicPrice + c;
            result += task.price + c;

            if (timesheet) {
            result += timesheet.price + c;
          } else {
            result += "No timesheet" + c;
          }
            result += task.owner + c;
            result += task.ownerName + c;
            result += task.paid + c;
            result += task.onHold + c;

            if (task.needsQuote !== undefined) {
              result += task.needsQuote + c;
            } else {
              result += "Task without Quote" + c;
            }

             console.log(result.toString("utf8"));

          });
        };


        tasks.forEach(function(task){

          // Get the time of the last created task
          findTimesheets(task);

        });

        setTimeout(function(){
            db.close();
        }, 3600*1);

    });

});
